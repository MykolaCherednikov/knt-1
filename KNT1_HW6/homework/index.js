function isEquals(a, b){
    return a===b;
}

function isBigger(a,b){
    return a > b;
}

function storeNames(...theArgs){
    let arr = [];
    for(const arg of theArgs) {
        arr.push(arg)
    }
    return arr;
}

function getDifference(a,b){
    if(a>b){
        return a-b;
    } else if(a<b){
        return b-a;
    } else{
        return 0;
    }
}

function negativeCount(arr){
    let total = 0;
    for (let i = 0; i < arr.length; i++) {
        if(arr[i] < 0) {
            total++;
        }
    }
    return total
}

function letterCount(a,b){
    let total = 0;
    for (let i = 0; i < a.length; i++) {
        if(a[i] === b){
            total++;
        }
    }
    return total
}

function countPoints(arr){
    let total = 0;
    let temp = 0;
    for(let i = 0; i < arr.length; i++){
      	let a = '', b = '';
        temp = arr[i].indexOf(':');
        for(let y = 0; y < temp; y++){
            a+=arr[i][y];
        }
        a = Number(a);
        for(let y = temp+1; y < arr[i].length; y++){
            b+=arr[i][y];
        }
        b = Number(b);
        if(a>b){
            total+=3;
        } else if(a===b){
            total++;
        }
      	
    }
    return total;
}

console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']))