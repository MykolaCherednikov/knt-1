const data = [
    {
      '_id': '5b5e3168c6bf40f2c1235cd6',
      'index': 0,
      'age': 39,
      'eyeColor': 'green',
      'name': 'Stein',
      'favoriteFruit': 'apple'
    },
    {
      '_id': '5b5e3168e328c0d72e4f27d8',
      'index': 1,
      'age': 38,
      'eyeColor': 'blue',
      'name': 'Cortez',
      'favoriteFruit': 'strawberry'
    },
    {
      '_id': '5b5e3168cc79132b631c666a',
      'index': 2,
      'age': 2,
      'eyeColor': 'blue',
      'name': 'Suzette',
      'favoriteFruit': 'apple'
    },
    {
      '_id': '5b5e31682093adcc6cd0dde5',
      'index': 3,
      'age': 19,
      'eyeColor': 'green',
      'name': 'George',
      'favoriteFruit': 'banana'
    }
  ];


function reverseNumber(num) {
    let number = num.toString();
    let reverse;
    if(number[0] !== '-'){
      reverse = Number(String(number).split('').reverse().join(''));
    }else{
      reverse = '-';
      for(let i = number.length-1; i > 0;i--){
        reverse += number[i];
      }
    }
    return reverse;
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    const transformedArray = [];
    forEach(arr, element => transformedArray.push(func(element)));
    return transformedArray;
}

function filter(arr, func) {
    const filteredArray = [];
    forEach(arr, element => {
      if (func(element)) {
        filteredArray.push(element);
      }
    });
    return filteredArray;
}

function getAdultAppleLovers(data) {
    let names = [];
    let filtered = [];
    filtered = filter(data, person => person.age > 18 && person.favoriteFruit === 'apple')
    for(let i = 0; i < filtered.length; i++){
      names[i] = filtered[i].name;
    }
    return names;
}

function getKeys(obj) {
    const array = [];
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        array.push(key);
      }
    }
  
    return array;
}

function getValues(obj) {
    const array = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      array.push(obj[key]);
    }
  }

  return array;
}

function showFormattedDate(dateObj) {
    let monthNames = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ];
      return `Date: ${dateObj.getDate()} of ${monthNames[dateObj.getMonth()]}, ${dateObj.getFullYear()}`
}

console.log(getAdultAppleLovers(data))